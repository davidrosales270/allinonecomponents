<?php
namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class QuoteItem extends Model {

    /**
     * The database used by the model
     * 
     * @var string
     */

     protected $table = 'quotes_items';

     /**
      * The attributes that are mass assignable
      */

      protected $fillable = [
          'quotes_id', 
          'part_no', 
          'manufacturer', 
          'quantity', 
          'target_price',
          'alternates',
          'rohs'
      ];


}