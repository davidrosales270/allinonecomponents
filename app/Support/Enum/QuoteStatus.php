<?php
namespace Vanguard\Support\Enum;

class QuoteStatus
{
    const SEEN = 'seen';
    const NEW = 'new';

    public static function lists()
    {
        return[
            self::SEEN => 'Seen',
            self::NEW => 'New'    
        ];
    }


}