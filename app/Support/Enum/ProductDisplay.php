<?php 

namespace Vanguard\Support\Enum;

class ProductDisplay
{

    const ALL = 'All';
    const EMPTY = 'Empty';

    public static function lists()
    {
        return [
                self::ALL => self::ALL,
                self::EMPTY => self::EMPTY
        ];
    }

}