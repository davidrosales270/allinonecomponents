<?php

if (! function_exists('settings')) {
    /**
     * Get / set the specified settings value.
     *
     * If an array is passed as the key, we will assume you want to set an array of values.
     *
     * @param  array|string  $key
     * @param  mixed  $default
     * @return mixed
     */
    function settings($key = null, $default = null)
    {
        if (is_null($key)) {
            return app('anlutro\LaravelSettings\SettingStore');
        }

        return app('anlutro\LaravelSettings\SettingStore')->get($key, $default);
    }
}

function linkURL($key, $value){
    return remove_accents($value) . '-' . $key;
}

function remove_accents( $string ) {
    $filter = array('&','%'.'^',')','(','*',"@",'!', '-', ',', ';','.', '_','+','"','/');
    $string = str_replace($filter,'',$string);
    $string = str_replace(' ', '-',$string);
    $string = strtolower($string);

    return $string;
}

function ShowImageProduct($image_name){

    $image = 'noimage.jpg';

    if($image_name != null || $image_name != ""){
       if(file_exists($_SERVER['DOCUMENT_ROOT'] .'upload/product/' . $image_name)){
            $image = $image_name;
        }
    }

    return $image;
}

function extractId($name){
    $extract = explode('-', $name);
    return end( $extract );
}