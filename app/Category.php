<?php
namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Category extends Model {

    /**
     * The database used by the model
     * 
     * @var string
     */

     protected $table = 'categories';

     /**
      * The attributes that are mass assignable
      */

      protected $fillable = [
          'name', 'parent_id'
      ];


}