<?php

/**
 * Authentication
 */

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', [
    'as' => 'auth.logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

// Allow registration routes only if registration is enabled.
if (settings('reg_enabled')) {
    Route::get('register', 'Auth\AuthController@getRegister');
    Route::post('register', 'Auth\AuthController@postRegister');
    Route::get('register/confirmation/{token}', [
        'as' => 'register.confirm-email',
        'uses' => 'Auth\AuthController@confirmEmail'
    ]);
}

// Register password reset routes only if it is enabled inside website settings.
if (settings('forgot_password')) {
    Route::get('password/remind', 'Auth\PasswordController@forgotPassword');
    Route::post('password/remind', 'Auth\PasswordController@sendPasswordReminder');
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');
}




/**
 * Other
 */

Route::get('dashboard', [
    'as' => 'dashboard',
    'uses' => 'DashboardController@index'
]);

Route::get('/', [
    'as' => 'site',
    'uses' => 'SiteController@index'
]);

Route::get('about-us', [
    'as' => 'about-us',
    'uses' => 'SiteController@about'
]);

Route::get('contact', [
    'as' => 'contact',
    'uses' => 'SiteController@contact'
]);

Route::get('line-card', [
    'as' => 'line_card',
    'uses' => 'SiteController@lineCard'
]);

Route::get('line-card/{manufacturer}', [
        'as' => 'line-card',
        'uses' => 'SiteController@lineCardResult'
]);

Route::any('search_quote',[
    'as' => 'site.search_quote',
    'uses' => 'SiteController@search_quote'
]);

Route::get('contact_quotes/{part_no}', [
    'as' => 'site.contact_quotes',
    'uses' => 'SiteController@contact_quotes'
]);
Route::get('save_quote/{part_no}', [
    'as' => 'site.save_quote',
    'uses' => 'SiteController@save_quote'
]);
Route::post('store_quote', [
    'as' => 'site.store',
    'uses' => 'SiteController@store_quote'
]);

Route::get('products', [
    'as' => 'products',
    'uses' => 'SiteController@products'
]);

Route::get('product/{product_name}', [
    'as' => 'product',
    'uses' => 'SiteController@product'
]);

Route::get('product-category/main-categories/{category}',[
        'as' => 'site.products_categories',
        'uses' => 'SiteController@categories'
    ]);
Route::get('import/{var}/{var2}', [
        'as' => 'site.import',
        'uses' => 'SiteController@import'
]);

Route::get('analytics', [
    'as' => 'analytics.home',
    'uses' => 'AnalyticsController@index'
]);
/**
 * User Profile
 */

Route::get('profile', [
    'as' => 'profile',
    'uses' => 'ProfileController@index'
]);

Route::get('profile/activity', [
    'as' => 'profile.activity',
    'uses' => 'ProfileController@activity'
]);

Route::put('profile/details/update', [
    'as' => 'profile.update.details',
    'uses' => 'ProfileController@updateDetails'
]);

Route::post('profile/avatar/update', [
    'as' => 'profile.update.avatar',
    'uses' => 'ProfileController@updateAvatar'
]);

Route::post('profile/avatar/update/external', [
    'as' => 'profile.update.avatar-external',
    'uses' => 'ProfileController@updateAvatarExternal'
]);

Route::put('profile/login-details/update', [
    'as' => 'profile.update.login-details',
    'uses' => 'ProfileController@updateLoginDetails'
]);

Route::put('profile/social-networks/update', [
    'as' => 'profile.update.social-networks',
    'uses' => 'ProfileController@updateSocialNetworks'
]);

Route::post('profile/two-factor/enable', [
    'as' => 'profile.two-factor.enable',
    'uses' => 'ProfileController@enableTwoFactorAuth'
]);

Route::post('profile/two-factor/disable', [
    'as' => 'profile.two-factor.disable',
    'uses' => 'ProfileController@disableTwoFactorAuth'
]);

Route::get('profile/sessions', [
    'as' => 'profile.sessions',
    'uses' => 'ProfileController@sessions'
]);

Route::delete('profile/sessions/{session}/invalidate', [
    'as' => 'profile.sessions.invalidate',
    'uses' => 'ProfileController@invalidateSession'
]);

/**
 * Stock Mangement
 */
Route::get('stock', [
    'as' => 'stock.list',
    'uses' => 'StockController@index'
]);

Route::get('stock/create', [
    'as' => 'stock.create',
    'uses' => 'StockController@create'
]);

Route::post('stock/store', [
    'as' => 'stock.store',
    'uses' => 'StockController@store'
]);

Route::get('stock/{product}/show', [
    'as' => 'stock.show',
    'uses' => 'StockController@view'
]);

Route::get('stock/{product}/edit', [
    'as' => 'stock.edit',
    'uses' => 'StockController@edit'
]);

Route::put('stock/{product}/update/details', [
    'as' => 'stock.update.details',
    'uses' => 'StockController@updateDetails'
]);

Route::delete('stock/{product}/delete',[
    'as' => 'stock.delete',
    'uses' => 'StockController@delete'
]);
/**
 * Quote Management
 */

 Route::get('quote', [
     'as' => 'quote.list',
     'uses' => 'QuotesController@index'
 ]);

 Route::get('quote/{quote}/show', [
    'as' => 'quote.show',
    'uses' => 'QuotesController@show'
]);
Route::delete('quote/{quote}/delete',[
    'as' => 'quote.delete',
    'uses' => 'QuotesController@delete'
]);

/**
 * Category Management
 */

 Route::get('category', [
     'as' => 'category.list',
     'uses' => 'CategoriesController@index'
 ]);

 Route::get('category/create', [
    'as' => 'category.create',
    'uses' => 'CategoriesController@create'
 ]);

 Route::post('category/store', [
    'as' => 'category.store',
    'uses' => 'CategoriesController@store'
 ]);

 Route::get('category/{category}/show', [
    'as' => 'category.show',
    'uses' => 'CategoriesController@view'
 ]);

 Route::get('category/{category}/edit', [
     'as' => 'category.edit',
     'uses' => 'CategoriesController@edit'
 ]);

 Route::put('category/{category}/update/details',[
     'as' => 'category.update.details',
     'uses' => 'CategoriesController@updateDetails'
 ]);

 Route::delete('category/{category}/delete', [
     'as' => 'category.delete',
     'uses' => 'CategoriesController@delete'
 ]);

/**
 * User Management
 */
Route::get('user', [
    'as' => 'user.list',
    'uses' => 'UsersController@index'
]);

Route::get('user/create', [
    'as' => 'user.create',
    'uses' => 'UsersController@create'
]);

Route::post('user/create', [
    'as' => 'user.store',
    'uses' => 'UsersController@store'
]);

Route::get('user/{user}/show', [
    'as' => 'user.show',
    'uses' => 'UsersController@view'
]);

Route::get('user/{user}/edit', [
    'as' => 'user.edit',
    'uses' => 'UsersController@edit'
]);

Route::put('user/{user}/update/details', [
    'as' => 'user.update.details',
    'uses' => 'UsersController@updateDetails'
]);

Route::put('user/{user}/update/login-details', [
    'as' => 'user.update.login-details',
    'uses' => 'UsersController@updateLoginDetails'
]);

Route::delete('user/{user}/delete', [
    'as' => 'user.delete',
    'uses' => 'UsersController@delete'
]);

Route::post('user/{user}/update/avatar', [
    'as' => 'user.update.avatar',
    'uses' => 'UsersController@updateAvatar'
]);

Route::post('user/{user}/update/avatar/external', [
    'as' => 'user.update.avatar.external',
    'uses' => 'UsersController@updateAvatarExternal'
]);

Route::post('user/{user}/update/social-networks', [
    'as' => 'user.update.socials',
    'uses' => 'UsersController@updateSocialNetworks'
]);

Route::get('user/{user}/sessions', [
    'as' => 'user.sessions',
    'uses' => 'UsersController@sessions'
]);

Route::delete('user/{user}/sessions/{session}/invalidate', [
    'as' => 'user.sessions.invalidate',
    'uses' => 'UsersController@invalidateSession'
]);

Route::post('user/{user}/two-factor/enable', [
    'as' => 'user.two-factor.enable',
    'uses' => 'UsersController@enableTwoFactorAuth'
]);

Route::post('user/{user}/two-factor/disable', [
    'as' => 'user.two-factor.disable',
    'uses' => 'UsersController@disableTwoFactorAuth'
]);



/**
 * Settings
 */

Route::get('settings', [
    'as' => 'settings.general',
    'uses' => 'SettingsController@general',
    'middleware' => 'permission:settings.general'
]);

Route::post('settings/general', [
    'as' => 'settings.general.update',
    'uses' => 'SettingsController@update',
    'middleware' => 'permission:settings.general'
]);

Route::get('settings/auth', [
    'as' => 'settings.auth',
    'uses' => 'SettingsController@auth',
    'middleware' => 'permission:settings.auth'
]);

Route::post('settings/auth', [
    'as' => 'settings.auth.update',
    'uses' => 'SettingsController@update',
    'middleware' => 'permission:settings.auth'
]);

