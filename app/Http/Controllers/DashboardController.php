<?php

namespace Vanguard\Http\Controllers;

use Vanguard\Repositories\Activity\ActivityRepository;
use Vanguard\Repositories\User\UserRepository;
use Vanguard\Repositories\Product\ProductRepository;
use Vanguard\Repositories\Quote\QuoteRepository;
use Vanguard\Support\Enum\UserStatus;
use Vanguard\Support\Enum\QuoteStatus;
use Auth;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var ActivityRepository
     */
    private $activities;
    /**
     * @var ProductRepository
     */
    private $product;

    /**
     * @var QuoteRepository
     */
    private $quote;

    /**
     * DashboardController constructor.
     * @param UserRepository $users
     * @param ActivityRepository $activities
     */
    public function __construct(UserRepository $users, ActivityRepository $activities, ProductRepository $product, QuoteRepository $quote)
    {
        $this->middleware('auth');
        $this->users = $users;
        $this->activities = $activities;
        $this->product = $product;
        $this->quote = $quote;
    }

    /**
     * Displays dashboard based on user's role.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::user()->hasRole('Admin')) {
            return $this->adminDashboard();
        }

        return $this->defaultDashboard();
    }

    /**
     * Displays dashboard for admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function adminDashboard()
    {
        $quotesPerMonth = $this->quote->countOfNewQuotesPerMonth(
            Carbon::now()->startOfYear(),
            Carbon::now()
        );

        $stats = [
            'new' => $this->quote->countByStatus(QuoteStatus::NEW),
            'total_product' => $this->product->count()
        ];

        $latestQuotes = $this->quote->latest();

        return view('dashboard.admin', compact('stats', 'latestQuotes', 'quotesPerMonth'));
    }

    /**
     * Displays default dashboard for non-admin users.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function defaultDashboard()
    {
        $activities = $this->activities->userActivityForPeriod(
            Auth::user()->id,
            Carbon::now()->subWeeks(2),
            Carbon::now()
        )->toArray();

        return view('dashboard.default', compact('activities'));
    }


}