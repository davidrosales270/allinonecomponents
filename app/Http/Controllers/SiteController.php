<?php 

namespace Vanguard\Http\Controllers;

use Vanguard\Repositories\Category\CategoryRepository;
use Vanguard\Repositories\Quote\QuoteRepository;
use Vanguard\Repositories\QuoteItem\QuoteItemRepository;
use Vanguard\Repositories\Product\ProductRepository;
use Vanguard\Http\Requests\Quote\CreateQuoteRequest;
use Vanguard\Support\Enum\QuoteStatus;
use Vanguard\Quote;
use Vanguard\QuoteItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Vanguard\Mailers\UserMailer;

/**
 * Class SiteController
 * @package Vanguard\Http\Controllers
 */
class SiteController extends Controller{

    /**
     * @var QuoteRepository
     */

    private $quote;

    /**
     * @var QuoteItemRepository
     */

     private $quoteItem;

     /**
      * @var CategoryRepository
      */
    private $category;

    /**
     * @var ProductRepository
     */

     private $product;

    public function __construct(QuoteRepository $quote, 
                                QuoteItemRepository $quoteItem, 
                                CategoryRepository $category,
                                ProductRepository $product){

        $this->quote = $quote;
        $this->quoteItem = $quoteItem;
        $this->category = $category;
        $this->product = $product;
    }
    /**
     * Display Home Page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('site.index');
    }
    /**
     * Displays About Us page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about(){
        return view('site.about');
    }

     /**
     * Displays Contact Page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function contact(Request $request){
        
        $quotes = $request->session()->get('quotes');
        
        return view('site.contact', compact('quotes'));
    }


    public function search_quote(Request $request){
        $data = $request->all();

        $products_found = $this->product->search($data['part_no']);

        if(count($products_found)){    
            $categories = $this->category->parents();
            $part_no = $data['part_no'];
            $quotes = array();
            return view('site.search_quote', compact('products_found', 'categories', 'part_no', 'quotes'));    
        }else{
            return redirect()->route('site.contact_quotes', $data['part_no'] );
        }
    }
    /** 
     * Store Part No. to display in Contact Form
     * @param Request $request
     * @param $part_no
     * @return redirect
     */
    public function contact_quotes(Request $request, $part_no){
        
        if(!empty($part_no)){
            $product_id = extractId($part_no);

            $product_get = $this->product->find($product_id);

            $request->session()->push('quotes', $product_get->part_no);
        }

        return redirect()->route('contact');
    }

    /**
     * 
     */
    public function save_quote(Request $request, $part_no){
        $request->session()->forget('quotes');

        if(!empty($part_no)){
            $product_id = extractId($part_no);

            $product_show = $this->product->find($product_id);

            $request->session()->push('quotes', $product_show->part_no);
        }

        $quotes = $request->session()->get('quotes');
        return view('site.contactOne', compact('quotes'));
    }

    /**
     * Store News Quotes in Database.
     * @param CreateQuoteRequest $request
     * @return mixed
     */
    public function store_quote(CreateQuoteRequest $request, UserMailer $mailer){
        $data = $request->all();
        $request->session()->forget('quotes');


        $quote['name'] = $data['name'];
        $quote['phone'] = $data['phone'];
        $quote['bussines_name'] = $data['bussinnes_name'];
        $quote['email'] = $data['email'];
        $quote['bussiness_type'] = $data['bussinnes_type'];
        $quote['status'] = QuoteStatus::NEW;

        $quote = $this->quote->create($quote);

        for($i=0; $i<count($data['part_no']); $i++){
            $quoteItem['part_no'] = $data['part_no'][$i];
            $quoteItem['manufacturer'] = $data['manufacturer'][$i];
            $quoteItem['quantity'] = $data['quantity'][$i];
            $quoteItem['target_price'] = $data['target_price'][$i];
            $quoteItem['alternates'] = $data['alternates'][$i];
            $quoteItem['rohs'] = $data['rohs'][$i];
            $quoteItem['quotes_id'] = $quote->id;

            $this->quoteItem->create($quoteItem);

        }

        $this->sendQuoteEmail($mailer, $data);

        return redirect()->route('contact')
        ->withSuccess('Thanks, Your form was submited successfully!');
 
    }

    /**
     * @param UserMailer $mailer
     * @param $user
     */
    private function sendQuoteEmail(UserMailer $mailer, $data)
    {
        $mailer->sendQuote($data);
    }


    /**
     * Display Page Products by Category.
     * @param string $category_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function categories($category_name)
    {
        $category_id = extractId($category_name);
        $category_details = $this->category->find($category_id);

        if($category_details){
            $categories = $this->category->parents();
            
            $products_category = $this->product->findByCategory($category_id);

            $category_details = $this->category->find($category_id);

            return view('site.category', compact('categories', 'products_category', 'category_details'));
        }else{
            return abort(404);
        }


    }

    /**
     * Displays Products View
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function products(){
        $categories = $this->category->parents();

        return view('site.products', compact('categories'));
    }

    /**
     * Display Product View
     * @param string $product_name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

     public function product(Request $request, $product_name) 
     {
         $product_id = extractId($product_name);

         $product_show = $this->product->find($product_id);

         if($product_show){
            $categories = $this->category->parents();
            $category = $this->category->find($product_show->category);
            $related_products = $this->product->related($product_show->category);
            $quotes = array();

            return view('site.product', compact('categories', 
                                                'product_show', 
                                                'category', 
                                                'related_products',
                                                'quotes'
                                            ));
         }else{
             return abort(404);
         }
     }

     /**
     * List all Manufacturers
     *@param string $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function lineCard(){
         $manufactures = $this->product->manufacture();

         $_manufactures = array();

         $titles = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');

         for($i=0; $i < count($titles); $i++){
             foreach($manufactures as $manufacturer){
                $letter = substr($manufacturer->manufacturer,0,1);

                 if(strtolower($letter) == $titles[$i]){
                     $_manufactures[$letter][] = $manufacturer->manufacturer;
                 }
             }
         }
         $categories = $this->category->parents();
         return view('site.line_card', compact('_manufactures', 'categories'));

     }
    /**
     *  List products by Manufacturers
     *@param string $manufacturer
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
     public function lineCardResult($manufacturer) {
            $products = $this->product->findByManufacturer($manufacturer);
            $categories = $this->category->parents();
            $quotes = array();
            return view('site.line_card_result', compact('products', 'manufacturer', 'categories', 'quotes'));
     }

    /**
     * Method to Import Products
     *@param string $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function import($name,$manufacturer)
    {
       if($name != ""){ 
            $products =  $this->product->findByName($name);
            $message = '';
            if(count($products))
            {
                $message = 'Product is already exist!';  
            }
            else{
                $product['part_no'] = $name;
                $product['category'] = 13;
                $product['manufacturer'] = $manufacturer;
                $product['description'] = '';

                $this->product->create($product);
                $message = 'Product ' . $name . ' was created successfully!' ;

            }
            return view('site.import', compact('message'));
        }else{
            return abort(404);
        }
    }

}