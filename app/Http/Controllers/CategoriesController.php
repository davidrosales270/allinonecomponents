<?php

namespace Vanguard\Http\Controllers;

use Vanguard\Repositories\Category\CategoryRepository;
use Vanguard\Http\Requests\Category\CreateCategoryRequest;
use Vanguard\Category;
use Auth;
use Authy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * Class CategoryController
 * @package Vanguard\Http\Controllers
 */
class CategoriesController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categories;

    /**
     * CantegoriesController constructor.
     * @param CategoryRepository $categories
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);
     
        $this->categories = $categories;
    }

    /**
     * Display paginated list of all categories.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $perPage = 20;

        $categories = $this->categories->paginate($perPage, Input::get('search'));
        

        return view('category.list', compact('categories'));
    }

    /**
     * Displays Category.
     *
     * @param Category $category
   
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view(Category $category)
    {
        return view('category.view', compact('category'));
    }

    /**
     * Displays form for creating a new category.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = $this->categories->all();
     
        return view('category.add', compact('categories'));
    }

    /**
     * Stores new category into the database.
     *
     * @param CreateCategoryRequest $request
     * @return mixed
     */
    public function store(CreateCategoryRequest $request)
    {  
            $data = $request->all();
            
            $category = $this->categories->create($data);

            return redirect()->route('category.list')
            ->withSuccess('Category created succesfully!');
    }

    /**
     * Displays edit category form.
     *
     * @param Category $category
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Category $category)
    {
        $edit = true;

        $categories = $this->categories->all();

        return view('category.edit',
            compact('edit', 'categories'));
    }

    /**
     * Updates category details.
     *
     * @param Category $category
     * @param UpdateDetailsRequest $request
     * @return mixed
     */
    public function updateDetails(Category $category, CreateCategoryRequest $request)
    {
        $this->categories->update($category->id, $request->all());
        
        return redirect()->back()
            ->withSuccess(trans('app.category_updated'));
    }
    /**
     * Removes the category from database.
     *
     * @param Category $category
     * @return $this
     */
    public function delete(Category $category)
    {
        $this->categories->delete($category->id);

        return redirect()->route('category.list')
            ->withSuccess('Category deleted sucessfully!');
    }

}