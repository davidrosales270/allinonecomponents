<?php

namespace Vanguard\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * Class AnalyticsController
 * @package Vanguard\Http\Controllers
 */
class AnalyticsController extends Controller
{

    /**
     * AnalyticsController constructor.
     * 
     */
    public function __construct()
    {
    }

    /**
     * Display Analytics page.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('analytics.index');
    }

}