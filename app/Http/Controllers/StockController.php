<?php 
namespace Vanguard\Http\Controllers;

use Vanguard\Repositories\Product\ProductRepository;
use Vanguard\Http\Requests\Product\CreateProductRequest;
use Vanguard\Repositories\Category\CategoryRepository;
use Vanguard\Product;
use Vanguard\Category;
use Auth;
use Authy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Vanguard\Support\Enum\ProductDisplay;

/**
 * Class StockController
 * @package Vanguard\Http\Controllers
 */
class StockController extends Controller
{

    /**
     * @var ProductRepository
     */

     private $products;

     /**
      * @var CategoryRepository
      */

      private $categories;

     public function __construct(ProductRepository $products, CategoryRepository $categories)
     {

        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);

        $this->products = $products;

        $this->categories = $categories;
     }

     /***
      * Display paginated list of all products
      * 
      * @return \Illuminate\Contracts\View\Factory\Illuminate\View\View
      */
      public function index()
      {
          $perPage = 20;

          $products = $this->products->paginate($perPage, Input::get('search'), Input::get('productDisplay'));
          $productDisplay = ProductDisplay::lists();  
          return view('product.list', compact('products', 'productDisplay'));
      }

      /**
       * Displays Category
       * 
       * @param Product $products
       * 
       * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
       */
      public function view(Product $product)
      {
          return view('product.view', compact('product'));
      }

      /**
       * Display form for creating a new product
       * 
       * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
       */
      public function create()
      {
          $categories = $this->categories->all();
          return view('product.add', compact('categories'));
      }

      /**
     * Stores new product into the database.
     *
     * @param CreateProductRequest $request
     * @return mixed
     */
      public function store(CreateProductRequest $request)
      {
          $data = $request->all();

          $product = $this->products->create($data);

          return redirect()->route('stock.list')
          ->withSuccess('Product created successfully!');
      }

    /**
     * Displays edit product form.
     *
     * @param Product $product
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Product $product)
    {
        $edit = true;
        $categories = $this->categories->all();
        return view('product.edit',
            compact('edit', 'categories', 'product'));
    }

    /**
     * Updates product details.
     *
     * @param Product $product
     * @param CreateProductRequest $request
     * @return mixed
     */
    public function updateDetails(Product $product, CreateProductRequest $request)
    {
        $this->products->update($product->id, $request->all());
        
        return redirect()->back()
            ->withSuccess(trans('Product was updated'));
    }
    /**
     * Removes the product from database.
     *
     * @param Product $product
     * @return $this
     */
    public function delete(Product $product)
    {
        $this->products->delete($product->id);

        return redirect()->route('stock.list')
            ->withSuccess('Product deleted successfully!');
    }

}
?>