<?php 

namespace Vanguard\Http\Controllers;

use Vanguard\Repositories\Quote\QuoteRepository;
use Vanguard\Repositories\QuoteItem\QuoteItemRepository;
use Vanguard\Quote;
use Vanguard\QuoteItem;
use Vanguard\Support\Enum\QuoteStatus;
use Auth;
use Authy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

/**
 * Class QuotesController
 * @package Vanguard\Http\Controllers
 */
class QuotesController extends Controller
{
    /**
     * @var QuoteRepository
     */
    private $quote;

    /**
     * @var QuoteItemRepository
     */
    private $quoteItem;

    public function __construct(QuoteRepository $quote, QuoteItemRepository $quoteItem)
    {
        $this->middleware('auth');
        $this->middleware('session.database', ['only' => ['sessions', 'invalidateSession']]);

        $this->quote = $quote;
        $this->quoteItem = $quoteItem;
    }

    /**
     * Display paginated list of all Quotes
     */
    public function index()
    {
        $perPage = 20;

        $quotes = $this->quote->paginate($perPage, Input::get('search'));
        return view('quotes.list', compact('quotes'));
    }

    /**
     * Display Quote
     */

     public function show(Quote $quote)
     {
         $this->quote->updateStatus($quote->id, QuoteStatus::SEEN);
         $quoteItems = $this->quoteItem->findByQuote($quote->id);
         return view('quotes.view', compact('quote', 'quoteItems'));
     }

     /**
      * Delete Quote with its Items
      */

      public function delete(Quote $quote)
      {
          $this->quoteItem->deleteByQuote($quote->id);
          $this->quote->delete($quote->id);

          return redirect()->route('quote.list')
          ->withSuccess('Quote deleted successfully!');
      }
}