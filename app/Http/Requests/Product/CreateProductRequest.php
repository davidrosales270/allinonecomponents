<?php 
namespace Vanguard\Http\Requests\Product;

use Vanguard\Http\Requests\Request;
use Vanguard\Product;

class CreateProductRequest extends Request
{
    /**
     * Get the validation rules that apply to the request
     * 
     * @return array
     */

     public function rules()
     {
       return [  
         'part_no' => 'required',
      
         'category' => 'required',
        
       ];

     }
}