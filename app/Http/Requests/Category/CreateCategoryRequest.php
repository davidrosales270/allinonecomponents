<?php 

namespace Vanguard\Http\Requests\Category;

use Vanguard\Http\Requests\Request;
use Vanguard\Category;

class CreateCategoryRequest extends Request
{

    /**
     * Get the validation rules that apply to the request
     * 
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'parent_id' => 'required'
        ];
    }
}