<?php 
namespace Vanguard\Http\Requests\Quote;

use Vanguard\Http\Requests\Request;
use Vanguard\Quote;
use Vanguard\QuoteItem;

class CreateQuoteRequest extends Request
{
    /**
     * Get the validation rules that apply to the request
     * 
     * @return array
     */

     public function rules()
     {
         return [
                'name' => 'required',
                'phone' => 'required',
                'bussines_name' => '',
                'email' => 'required',
                'bussiness_type' => '',
                'part_no' => '',
                'manufacturer' => '',
                'quantity' => '',
                'target_price' => '',
                'alternates' => '',
                'rohs' => ''

         ];
     }


}