<?php
namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    /**
     * The database used by the model
     * 
     * @var string
     */

     protected $table = 'products';


     /**
      * The attributes that are mass assignable
      */

     protected $fillable = [
          'part_no',
          'meta_name',
          'manufacturer',
          'description',
          'image',
          'category'
      ];

}