<?php
namespace Vanguard;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model {

    /**
     * The database used by the model
     * 
     * @var string
     */

     protected $table = 'quotes';

     /**
      * The attributes that are mass assignable
      */

      protected $fillable = [
          'name', 
          'phone',
          'bussines_name',
          'email',
          'bussiness_type',
          'status'
      ];


}