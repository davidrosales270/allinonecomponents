<?php 

namespace Vanguard\Repositories\QuoteItem;

use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Vanguard\QuoteItem;

class EloquentQuoteItem implements QuoteItemRepository
{

    public function __construct()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return QuoteItem::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return QuoteItem::create($data);
    }

    public function findByQuote($quote_id)
    {
        $query = QuoteItem::query();
        $query->where(function($q) use($quote_id){
            $q->where("quotes_id", "=", $quote_id);
        });
        $result = $query->get();

        return $result;
    }

    /**
     * {@inheritdoc}
     */

    public function deleteByQuote($quote_id)
    {
        $query = QuoteItem::query();

        $query->where('quotes_id', '=',  $quote_id)->delete();
    }

}