<?php 
namespace Vanguard\Repositories\QuoteItem;

use Vanguard\QuoteItem;

interface QuoteItemRepository
{
    
    /**
     * Find quoteitems by its id
     * @param $id
     * @return null QuoteItems
     */

    public function find($id);

    /**
     * Create new quoteitems
    *
    * @param array $data
    * @return mixed
    */

    public function create(array $data);

    /**
     * Find quoteitems by its id
     * @param $id
     * @return null QuoteItems
     */

    public function findByQuote($id);

    /**
     * Delete quoteitems by quote id
     * @param $quote_id
     * @return null 
     */

     public function deleteByQuote($quote_id);

    
}