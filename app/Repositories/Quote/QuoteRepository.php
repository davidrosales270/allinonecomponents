<?php

namespace Vanguard\Repositories\Quote;

use Vanguard\Quote;

interface QuoteRepository
{

    /**
     * paginate Quotes
     * 
     * @param $perPage
     * @param null search
     * @retun mixed
     */

    public function paginate($perPage, $search = null);

    /**
     * Find Quotes by its id
     * @param $id
     * @return null Product
     */

    public function find($id);

    /**
    * Create new quote
    *
    * @param array $data
    * @return mixed
    */

    public function create(array $data);

    /**
    * Update quote by its id
    * @param $id
    * @param array $data
    * @retun mixed
    */

    public function update($id, array $data);

    /**
     * Delete quote with provided id
     * 
     * @param $id
     * @return mixed
     */

    public function delete($id);

    /**
    * Number of quotes in database
    * @return mixed
    */

    public function count();

    /**
     * Latest Quotes
     */

     public function latest();

     /**
     * Get Quotes by Status
     * @param $status
     * @return mixed
     */

    public function countByStatus($status);

    /**
     * Get Quotes by Range Date
     * @param $from
     * @param $to
     * @return mixed
     */

    public function countOfNewQuotesPerMonth($from, $to);

    /**
     * Update Quote Status
     * @param $quote_id
     * @param $status
     * @return mixed
     */
    public function updateStatus($quote_id, $status);
}