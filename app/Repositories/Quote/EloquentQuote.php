<?php 
namespace Vanguard\Repositories\Quote;

use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Vanguard\Quote;

class EloquentQuote implements QuoteRepository
{
    public function __construct()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Quote::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Quote::create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($perPage, $search = null){
        
        $query = Quote::query();

        if($search){
            $query->where(function($q) use($search){
                $q->where('name', "like", "%{$search}%");
            });
        }

        $result = $query->orderBy('created_at', 'DESC')->paginate($perPage);

        if($search){
            $result->appends(['search' => $search]);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        return $this->find($id)->update($data);
    }

    /**
     *  {@inheritdoc}
     */
    public function delete($id)
    {
        $product = $this->find($id);
        $product->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return Quote::count();
    }

    /**
     * {@inheritdoc}
     */

     public function latest()
     {
         return

         Quote::orderBy('created_at', 'DESC')
         ->limit(5)
         ->get();
     }

     /**
      * {@inheritdoc}
      */
    public function countByStatus($status)
    {
        return Quote::where('status', $status)->count();

    }

    /**
    * {@inheritdoc}
    */
   public function countOfNewQuotesPerMonth($from, $to)
   {
       $perMonthQuery = $this->getPerMonthQuery();

       $result = Quote::select([
           DB::raw("{$perMonthQuery} as month"),
           DB::raw('count(id) as count')
       ])
           ->whereBetween('created_at', [$from, $to])
           ->groupBy('month')
           ->orderBy('month', 'ASC')
           ->lists('count', 'month');

       $counts = [];

       foreach(range(1, 12) as $m) {
           $month = date('F', mktime(0, 0, 0, $m, 1));

           $month = trans("app.months.{$month}");

           $counts[$month] = isset($result[$m])
               ? $result[$m]
               : 0;
       }

       return $counts;
   }

   /**
    * {@inheritdoc}
    */
    public function updateStatus($quote_id, $status)
    {
        $data = array('status' => $status);
        return $this->find($quote_id)->update($data);
    }

   /**
     * Creates query that will be used to fetch users per
     * month, depending on type of the connection.
     *
     * @return string
     */
    private function getPerMonthQuery()
    {
        $connection = DB::connection();

        if ($connection instanceof SQLiteConnection) {
            return 'CAST(strftime(\'%m\', created_at) AS INTEGER)';
        }

        return 'MONTH(created_at)';
    }


}