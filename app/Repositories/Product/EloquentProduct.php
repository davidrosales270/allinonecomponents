<?php 
namespace Vanguard\Repositories\Product;

use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Vanguard\Product;
use Illuminate\Support\Facades\Input;
use Vanguard\Support\Enum\ProductDisplay;

class EloquentProduct implements ProductRepository
{

    public function __construct()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Product::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Product::create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($perPage, $search = null, $productDisplay = null){

        $query = Product::query();

        if($search){
            $query->where(function($q) use($search){
                $q->where('part_no', "like", "%{$search}%");
            }); 
        }

        if($productDisplay == ProductDisplay::EMPTY)
        {
            $query->where(function($q) use($productDisplay){

                $q->where('manufacturer', "=", "");
            });
        }

        $result = $query->paginate($perPage);

        if($search){
            $result->appends(['search' => $search]);
        }

        if($productDisplay){
            $result->appends(['productDisplay' => $productDisplay]);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        return $this->find($id)->update($data);
    }

    /**
     *  {@inheritdoc}
     */
    public function delete($id)
    {
        $product = $this->find($id);
        $product->delete();
    }


    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return Product::count();
    }

    /**
     * {@inheritdoc}
     */
    public function latest($count = 20)
    {
        return Product::orderBy("created_at", 'DESC')
        ->limit($count)
        ->get();
    }

    /**
     * {@inheritdoc}
     */

     public function all($column = 'name', $key = 'id')
     {
         return Product::orderBy('name')->lists($column,$key);
     }

     /**
     * {@inheritdoc}
     */

     public function findByName($name)
     {
         return Product::where('part_no', $name)->get();
     }

     /**
     * {@inheritdoc}
     */

     public function findByCategory($category_id)
     {
        return Product::where('category', $category_id)->paginate(10);
         
     }

     /**
      * {@inheritdoc}
      */

    public function related($category_id)
    {
        return Product::where('category', $category_id)->orderBy(DB::raw('RAND()'))->limit(4)->get();
    }

    /**
     * {@inheritdoc}
     */

     public function search($search)
     {
         $products = Product::where('part_no', 'like', '%' . $search . '%')->paginate(10);
         $pagination = $products->appends ( array (
                            'part_no' => Input::get ( 'part_no' ) 
                        ) );

         return $products;
     }

     public function manufacture(){
         $products = Product::where('manufacturer', '!=', '')
                    ->groupBy('manufacturer')
                    ->orderBy('manufacturer', 'ASC')
                    ->get();
        return $products;
     }

     public function findByManufacturer($manufacturer){
         $products = Product::where('manufacturer', '=', $manufacturer)->paginate(10);

         $pagination = $products->appends(
                    array(
                        'manufacturer' => Input::get( 'manufacturer' )
                    )
         );

         return $products;
     }
}