<?php 

namespace Vanguard\Repositories\Product;

use Vanguard\Product;

interface ProductRepository
{

    /**
     * paginate products
     * 
     * @param $perPage
     * @param null search
     * @retun mixed
     */

    public function paginate($perPage, $search = null, $productDisplay = null);

    /**
     * Find Product by its id
     * @param $id
     * @return null Product
     */

     public function find($id);

     /**
      * Create new product
      *
      * @param array $data
      * @return mixed
      */

     public function create(array $data);

     /**
      * Update product by its id
      * @param $id
      * @param array $data
      * @retun mixed
      */

      public function update($id, array $data);


      /**
       * Delete product with provided id
       * 
       * @param $id
       * @return mixed
       */

       public function delete($id);

       /**
        * Number of products in database
        * @return mixed
        */

        public function count();

        /**
         * 
         * Get latest {$count} products from database
         */

         public function latest($count = 10);

        /**
         * Find Product by its name
         * @param $name
         * @return mixed
         */

        public function findByName($name);

        
        /**
         * Find Products by category_id
         * @param $category_id
         * @return mixed
         */

        public function findByCategory($category_id);

        /**
         * Find Products Related
         * @param $category_id
         * @return mixed
         */
        public function related($category_id);

        /**
         * 
         * Search Products by title
         * @param $search
         * @return mixed
         */

         public function search($search);

         /**
          * 
          */
          public function manufacture();

          /**
           * 
           */
        public function findByManufacturer($manufacturer);

}