<?php 

namespace Vanguard\Repositories\Category;

use Vanguard\Category;

interface CategoryRepository
{
    /**
     * Paginate Categories
     * @param $perPage
     * @param null search
     * @return mixed
     */

     public function paginate($perPage, $search = null);

     /**
      * Find Category by its id
      *@param $id
      *@return null|Category
      */

      public function find($id);

      /**
       * Create new category
       * 
       * @param array $data
       * @return mixed
       */

       public function create(array $data);

       /**
        * Update category by its id
        * @param $id
        * @param array $data
        * @return mixed
        */

        public function update($id, array $data);

        /**
         * 
         * Delete category with provided id
         * 
         * @param $id
         * @return mixed
         */

        public function delete($id);

        /**
         * Number of categories in database
         * @return mixed
         */

        public function count();

        /**
         * Get latest {$count} categories from database
         * 
         * @param count
         * @return mixed
         */

         public function latest($count = 20);

         /**
          * Create $key => $value for all categories 
          *
          * @param string $column
          * @param string $key
          * @return mixed
          */

          public function all($column = 'name', $key='id');

          /**
          * Create $key => $value for parents categories 
          *
          * @param string $column
          * @param string $key
          * @return mixed
          */
          public function parents($column = 'name', $key='id');

         
}