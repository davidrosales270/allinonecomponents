<?php 

namespace Vanguard\Repositories\Category;

use Carbon\Carbon;
use DB;
use Illuminate\Database\SQLiteConnection;
use Vanguard\Category;

class EloquentCategory implements CategoryRepository
{

    public function __construct()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function find($id)
    {
        return Category::find($id);
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $data)
    {
        return Category::create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function paginate($perPage, $search = null){

        $query = Category::query();

        if($search){
            $query->where(function($q) use($search){
                $q->where('name', "like", "%{$search}%");
               
            });
        }

        $query->where(function($q) use($search){
            $q->where('id', '!=' , "1");
        });

        $result = $query->paginate($perPage);

        if($search){
            $result->appends(['search' => $search]);
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function update($id, array $data)
    {
        return $this->find($id)->update($data);
    }

    /**
     *  {@inheritdoc}
     */
    public function delete($id)
    {
        $category = $this->find($id);
        $category->delete();
    }


    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return Category::count();
    }

    /**
     * {@inheritdoc}
     */
    public function latest($count = 20)
    {
        return Category::orderBy("created_at", 'DESC')
        ->limit($count)
        ->get();
    }

    /**
     * {@inheritdoc}
     */

     public function all($column = 'name', $key = 'id')
     {
         return Category::orderBy('id')->lists($column,$key);
     }

     /**
      * {@inheritdoc}
      */

    public function parents($column = 'name', $key='id')
    {
        return Category::where('parent_id', '1')
                        ->orderBy('id', 'asc')
                        ->get();
    }
}

