@extends('layouts.app')

@section('page-title', 'Edit Product')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit product
            <small>Product Details</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li><a href="{{ route('stock.list') }}">Products</a></li>
                    <li class="active">@lang('app.create')</li>
                </ol>
            </div>
        </h1>
    </div>
</div>

@include('partials.messages')

{!! Form::open(['route' => ['stock.update.details', $product->id], 'method' => 'PUT', 'id' => 'user-form']) !!}
    <div class="row">
        <div class="col-md-8">
            @include('product.partials.details', ['edit' => true , 'product' => $product, 'categories' => $categories])
        </div>
        <div class="col-md-4">
           
        </div>
    </div>
    @if(!$edit)
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                    Create Product
                </button>
            </div>
        </div>
    @endif
{!! Form::close() !!}

@stop

@section('scripts')
    {!! HTML::script('assets/js/moment.min.js') !!}
    {!! JsValidator::formRequest('Vanguard\Http\Requests\Product\CreateProductRequest', '#user-form') !!}
@stop