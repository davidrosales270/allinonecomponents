@extends('layouts.app')

@section('page-title', trans('app.users'))

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Products
            <small>List of Products</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">Products</li>
                </ol>
            </div>

        </h1>
    </div>
</div>

@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2">
        <a href="{{ route('stock.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
            Add Product
        </a>
    </div>
    <div class="col-md-5"></div>
    <form method="GET" action="" accept-charset="UTF-8" id="users-form">
        <div class="col-md-2">
            <label for="productDisplay">
                Manufacturer
            </label>
            {!! Form::select('productDisplay', $productDisplay, Input::get('productDisplay'), ['id' => 'productDisplay', 'class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
        <label for="search">
                Search
            </label>
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" id="search" name="search" value="{{ Input::get('search') }}" placeholder="Search for Products">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (Input::has('search') && Input::get('search') != '')
                        <a href="{{ route('stock.list') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>
</div>

<div class="table-responsive top-border-table" id="users-table-wrapper">
    <table class="table">
        <thead>
            <th>#</th>
            <th>Part No.</th>
            <th>Manufacturer</th>
            <th class="text-center">@lang('app.action')</th>
        </thead>
        <tbody>
            @if (count($products))
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id ?: trans('app.n_a') }}</td>
                        <td>{{ $product->part_no }}</td>
                        <td>{{ $product->manufacturer }}</td>
                        <td class="text-center">
                            <!--<a href="{{ route('stock.show', $product->id) }}" class="btn btn-success btn-circle"
                               title="View product" data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>-->
                            <a href="{{ route('stock.edit', $product->id) }}" class="btn btn-primary btn-circle edit" title="Edit Product"
                                    data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                            <a href="{{ route('stock.delete', $product->id) }}" class="btn btn-danger btn-circle" title="Delete product"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-method="DELETE"
                                    data-confirm-title="@lang('app.please_confirm')'"
                                    data-confirm-text="@lang('app.are_you_sure_delete_user')"
                                    data-confirm-delete="@lang('app.yes_delete_him')'">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6"><em>@lang('app.no_records_found')</em></td>
                </tr>
            @endif
        </tbody>
    </table>

    {!! $products->render() !!}
</div>

@stop

@section('scripts')
    <script>
        $("#status").change(function () {
            $("#users-form").submit();
        });
    </script>
@stop
