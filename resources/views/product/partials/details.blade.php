<div class="panel panel-default">
    <div class="panel-heading">Product Details</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Part No.</label>
                    <input type="text" class="form-control" id="part_no"
                           name="part_no" placeholder="Part No." value="{{ $edit ? $product->part_no : '' }}">
                </div>

                <div class="form-group">
                    <label for="name">Manufacturer</label>
                    <input type="text" class="form-control" id="manufacturer"
                           name="manufacturer" placeholder="Manufacturer" value="{{ $edit ? $product->manufacturer : '' }}">
                </div>

                <div class="form-group">
                    <label for="name">Description</label>
                    <textarea class="form-control" id="description" name="description">{{ $edit ? $product->description : '' }}</textarea>
                </div>

                

                <div class="form-group">
                    <label for="category">Category</label>
                    {!! Form::select('category',  $categories, $edit ? $product->category : '',
                        ['class' => 'form-control', 'id' => 'category']) !!}
                </div>
                
            </div>

            

            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        @lang('app.update_details')
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>