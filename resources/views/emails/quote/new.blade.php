<html>
<head>
<title>New Quote</title>
<style>
    body{
        color: #333;
        font-size: 12px;
        font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
    }
    img {
        display: block;
        margin: 0 auto;
    }
    hr.line{
        
        border: 5px solid #0081db;
    }
    .items-products {
        border-spacing: 0px;
        border-collapse: collapse;
        margin-top: 10px;
    }
    .items-products th{
        background: #f6f6f6;
        padding: 5px;
    }
    .items-products tbody tr{
        border-bottom: 1px solid #e0e0e0;
        text-align: center;
    }
</style>
</head>
<body>
    <img src="http://www.allinonecomponents.com/assets/img/vanguard-logo.png" width="300px" alt="">

    <hr class="line"> 

    <table class="user-information" >
        <tr>
            <td>Name:</td>
            <td>{{ $data['name'] }}</td>
        </tr>
        <tr>
            <td>Phone Number:</td>
            <td>{{ $data['phone'] }}</td>
        </tr>
        <tr>
            <td>Email:</td>
            <td>{{ $data['email'] }}</td>
        </tr>
        <tr>
            <td>Businness Name:</td>
            <td>{{$data['bussinnes_name']}}</td>
        </tr>
        <tr>
            <td>Businness Type:</td>
            <td>{{ $data['bussinnes_type'] }}</td>
        </tr>
    </table>

    <table width="100%" class="items-products" >
        <thead>
            <th>Part No.</th>
            <th>Manufacturer</th>
            <th>Quantity</th>
            <th>Target Price</th>
            <th>Alternates</th>
            <th>RoHS</th>
        </thead>
        <tbody>
            @for($i=0; $i<count($data['part_no']); $i++)
                <tr>
                    <td>{{ $data['part_no'][$i] }}</td>
                    <td>{{ $data['manufacturer'][$i] }}</td>
                    <td>{{ $data['quantity'][$i] }}</td>
                    <td>{{ $data['target_price'][$i] }}</td>
                    <td>{{ $data['alternates'][$i] }}</td>
                    <td>{{ $data['rohs'][$i] }}</td>
                </tr>
            @endfor
        </tbody>
    </table>
</body>
</html>