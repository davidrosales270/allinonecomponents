@extends('layouts.app')

@section('page-title', trans('app.dashboard'))

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            @lang('app.welcome') <?= Auth::user()->username ?: Auth::user()->first_name ?>!
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">@lang('app.dashboard')</li>
                </ol>
            </div>

        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-widget panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-7">
                        <div class="title">New Quotes</div>
                        <div class="text-huge">{{ $stats['new'] }}</div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-file fa-5x"></i>
                    </div>
                </div>
            </div>
            <a href="{{ route('quote.list') }}">
                <div class="panel-footer">
                    <span class="pull-left">View all Quotes</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-widget panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-7">
                        <div class="title">Total Products</div>
                        <div class="text-huge">{{ $stats['total_product'] }}</div>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cube fa-5x"></i>
                    </div>
                </div>
            </div>
            <a href="{{ route('stock.list') }}">
                <div class="panel-footer">
                    <span class="pull-left">View Products</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-8">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('app.registration_history')</div>
            <div class="panel-body">
                <div>
                    <canvas id="myChart" height="403"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Latest Quotes</div>
            <div class="panel-body">
                @if (count($latestQuotes))
                    <div class="list-group">
                        @foreach ($latestQuotes as $quote)
                            <a href="{{ route('quote.show', $quote->id) }}" class="list-group-item">
                                
                                &nbsp; <strong>{{ $quote->name }}</strong>
                                <span class="list-time text-muted small">
                                    <em>{{ $quote->created_at->diffForHumans() }}</em>
                                </span>
                            </a>
                        @endforeach
                    </div>
                    <a href="{{ route('quote.list') }}" class="btn btn-default btn-block">View all Quotes</a>
                @else
                    <p class="text-muted">@lang('app.no_records_found')</p>
                @endif
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    <script>
        var users = {!! json_encode(array_values($quotesPerMonth)) !!};
        var months = {!! json_encode(array_keys($quotesPerMonth)) !!};
        var trans = {
            chartLabel: "Quotes History",
            new: "",
            user: "new quote",
            users: "new quotes"
        };
    </script>
    {!! HTML::script('assets/js/chart.min.js') !!}
    {!! HTML::script('assets/js/as/dashboard-admin.js') !!}
@stop