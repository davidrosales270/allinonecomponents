<!doctype html>
<html lang="en-US">
<head>
<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8" />

<title>@yield('page-title')</title>
<meta name="keywords" content="allinone, electronic components,electronic component distributor,electronic component supplier,electronic component parts,buy electronic components" />
<meta name="description" content="Electronic Components - AllInOne Electronic Components provides industry-leading contract manufacturing, design, and engineering services" />

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

<!-- CSS
================================================== -->

{!! HTML::style('assets/site/css/style.css') !!}
{!! HTML::style('assets/site/css/colors/blue.css') !!}
@yield('styles')
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-81402983-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-81402983-1');
</script>

<!-- Hotjar Tracking Code for http://www.allinonecomponents.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:786452,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</head>
<body>

<div id="wrapper">
    @include('partials.header')
    
    @yield('content')

    @include('partials.footer')
</div>

<!-- Scripts
================================================== -->
{!! HTML::script('/assets/site/scripts/jquery-2.2.0.min.js') !!}
{!! HTML::script('/assets/site/scripts/superfish.js') !!}
{!! HTML::script('/assets/site/scripts/hoverIntent.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.flexslider-min.js') !!}
{!! HTML::script('/assets/site/scripts/owl.carousel.min.js') !!}
{!! HTML::script('/assets/site/scripts/counterup.min.js') !!}
{!! HTML::script('/assets/site/scripts/waypoints.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.themepunch.tools.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.themepunch.revolution.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.isotope.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.magnific-popup.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.sticky-kit.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.twentytwenty.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.event.move.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.photogrid.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.tooltips.min.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.pricefilter.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.stacktable.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.contact-form.js') !!}
{!! HTML::script('/assets/site/scripts/jquery.jpanelmenu.js') !!}
{!! HTML::script('/assets/site/scripts/custom.js') !!}
@yield('scripts')

</body>
</html>