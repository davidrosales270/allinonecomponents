<div class="panel panel-default">
    <div class="panel-heading">Category Details</div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                
                
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name"
                           name="name" placeholder="Name" value="{{ $edit ? $category->name : '' }}">
                </div>

                <div class="form-group">
                    <label for="parent_id">Category Parent</label>
                   
                    
                    {!! Form::select('parent_id',  $categories, $edit ? $category->parent_id : '',
                        ['class' => 'form-control', 'id' => 'parent_id']) !!}
                </div>
                
            </div>

            

            @if ($edit)
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary" id="update-details-btn">
                        <i class="fa fa-refresh"></i>
                        @lang('app.update_details')
                    </button>
                </div>
            @endif
        </div>
    </div>
</div>