@extends('layouts.app')

@section('page-title', trans('app.users'))

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Categories
            <small>List of Categories</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">Categories</li>
                </ol>
            </div>

        </h1>
    </div>
</div>

@include('partials.messages')

<div class="row tab-search">
    <div class="col-md-2">
        <a href="{{ route('category.create') }}" class="btn btn-success" id="add-user">
            <i class="glyphicon glyphicon-plus"></i>
            Add Category
        </a>
    </div>
    <div class="col-md-5"></div>
    <form method="GET" action="" accept-charset="UTF-8" id="users-form">
        <div class="col-md-3">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ Input::get('search') }}" placeholder="Search for categories">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (Input::has('search') && Input::get('search') != '')
                        <a href="{{ route('category.list') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>
</div>

<div class="table-responsive top-border-table" id="users-table-wrapper">
    <table class="table">
        <thead>
            <th>#</th>
            <th>Category</th>
            <th class="text-center">@lang('app.action')</th>
        </thead>
        <tbody>
            @if (count($categories))
                @foreach ($categories as $category)
                    <tr>
                        <td>{{ $category->id ?: trans('app.n_a') }}</td>
                        <td>{{ $category->name }}</td>
                        <td class="text-center">
                           <!-- <a href="{{ route('category.show', $category->id) }}" class="btn btn-success btn-circle"
                               title="Ver Category" data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>-->
                            <!--<a href="{{ route('category.edit', $category->id) }}" class="btn btn-primary btn-circle edit" title="Edit Category"
                                    data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>-->
                            <a href="{{ route('category.delete', $category->id) }}" class="btn btn-danger btn-circle" title="Delete Category"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-method="DELETE"
                                    data-confirm-title="@lang('app.please_confirm')'"
                                    data-confirm-text="Are you sure delete this Category?"
                                    data-confirm-delete="Yes, Delete it!">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6"><em>@lang('app.no_records_found')</em></td>
                </tr>
            @endif
        </tbody>
    </table>

    {!! $categories->render() !!}
</div>

@stop

@section('scripts')
    <script>
        $("#status").change(function () {
            $("#users-form").submit();
        });
    </script>
@stop
