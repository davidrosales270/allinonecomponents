@extends('layouts.app')

@section('page-title', "Add Category")

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Create new category
            <small>Category Details</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li><a href="{{ route('category.list') }}">Categories</a></li>
                    <li class="active">@lang('app.create')</li>
                </ol>
            </div>
        </h1>
    </div>
</div>

@include('partials.messages')

{!! Form::open(['route' => 'category.store', 'files' => true, 'id' => 'user-form']) !!}
    <div class="row">
        <div class="col-md-8">
           @include('category.partials.details', ['edit' => false])
        </div>
        
    </div>

    <div class="row">
        <div class="col-md-12">
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-save"></i>
                Create Category
            </button>
        </div>
    </div>
{!! Form::close() !!}

@stop

@section('scripts')
    {!! HTML::script('assets/js/moment.min.js') !!}
    {!! JsValidator::formRequest('Vanguard\Http\Requests\Category\CreateCategoryRequest', '#user-form') !!}
@stop