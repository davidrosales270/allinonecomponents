@extends('layouts.app')

@section('page-title', 'Quote')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotes
            <small>View Quote</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">Quotes</li>
                </ol>
            </div>

        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-6">
        <div id="edit-user-panel" class="panel panel-default">
            <div class="panel-heading">
                Client Information
            </div>
            <div class="panel-body panel-profile">
               

                <table class="table ">
                
                    <tbody>
                        <tr>
                            <td>@lang('app.name')</td>
                            <td>{{ $quote->name }}</td>
                        </tr>
                        <tr>
                            <td>@lang('app.email')</td>
                            <td><a href="mailto:{{ $quote->email }}">{{ $quote->email }}</a></td>
                        </tr>
                        @if ($quote->phone)
                            <tr>
                                <td>@lang('app.phone')</td>
                                <td><a href="telto:{{ $quote->phone }}">{{ $quote->phone }}</a></td>
                            </tr>
                        @endif
                        @if ($quote->bussines_name)
                            <tr>
                                <td>Bussiness name</td>
                                <td>{{ $quote->bussines_name }}</td>
                            </tr>
                        @endif

                        @if ($quote->bussiness_type)
                            <tr>
                                <td>Bussiness Type</td>
                                <td>{{ $quote->bussiness_type }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12">
        <table class="table">
            <thead>
                <tr>
                    <th>Part No.</th>
                    <th>Manufacturer</th>
                    <th>Qty.</th>
                    <th>Target Price</th>
                    <th>Alternates</th>
                    <th>RoHs</th>
                </tr>
            </thead>
            <tbody>
                @if(count($quoteItems))
                    @foreach($quoteItems as $item)
                    <tr>
                        <td><b>{{ $item->part_no }}</b></td>
                        <td>
                            @if($item->manufacturer)
                                {{ $item->manufacturer }}
                            @else
                                ----
                            @endif
                        </td>
                        <td>
                            @if($item->quantity)
                                {{ $item->quantity }}
                            @else
                                ----
                            @endif
                        </td>
                        <td>
                            @if($item->target_price)
                                {{ $item->target_price }}
                            @else
                                ----
                            @endif
                       </td>
                        <td>
                            @if($item->alternates)
                                {{ $item->alternates }}
                            @else
                                ----
                            @endif
                        </td>
                        <td>
                            @if($item->rohs)
                                {{ $item->rohs }}
                            @else
                                ----
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @else
                <tr>
                    <td colspan="6">There are not Items</td>
                </tr>
                @endif
            </tbody>

        </table>
    </div>
</div>
@stop