@extends('layouts.app')

@section('page-title', 'Quotes')

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Quotes
            <small>List of Quotes</small>
            <div class="pull-right">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">@lang('app.home')</a></li>
                    <li class="active">Quotes</li>
                </ol>
            </div>

        </h1>
    </div>
</div>


@include('partials.messages')

<div class="row tab-search">
    
    <div class="col-md-9"></div>
    <form method="GET" action="" accept-charset="UTF-8" id="users-form">
        <div class="col-md-3">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" value="{{ Input::get('search') }}" placeholder="Search for Quotes">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" id="search-users-btn">
                        <span class="glyphicon glyphicon-search"></span>
                    </button>
                    @if (Input::has('search') && Input::get('search') != '')
                        <a href="{{ route('stock.list') }}" class="btn btn-danger" type="button" >
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    @endif
                </span>
            </div>
        </div>
    </form>
</div>

<div class="table-responsive top-border-table" id="users-table-wrapper">
    <table class="table">
        <thead>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Date</th>
            <th>Status</th>
            <th class="text-center">@lang('app.action')</th>
        </thead>
        <tbody>
            @if (count($quotes))
                @foreach ($quotes as $quote)
                    <tr>
                        <td>{{ $quote->id ?: trans('app.n_a') }}</td>
                        <td>{{ $quote->name }}</td>
                        <td>{{ $quote->email }}</td>
                        <td>{{ $quote->created_at }}</td>
                        <td><div class="status_quote status_quote_{{ $quote->status }}"></div></td>
                        <td class="text-center">
                            <a href="{{ route('quote.show', $quote->id) }}" class="btn btn-success btn-circle"
                               title="View Quote" data-toggle="tooltip" data-placement="top">
                                <i class="glyphicon glyphicon-eye-open"></i>
                            </a>
                            <a href="{{ route('quote.delete', $quote->id) }}" class="btn btn-danger btn-circle" title="Delete Quote"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    data-method="DELETE"
                                    data-confirm-title="@lang('app.please_confirm')'"
                                    data-confirm-text="Are you sure delete this quote ?"
                                    data-confirm-delete="Yes, Delete it!">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6"><em>@lang('app.no_records_found')</em></td>
                </tr>
            @endif
        </tbody>
    </table>
    
    {!! $quotes->render() !!}

    <ul class="status_options">
        <li>
            <div class="status_quote status_quote_new"></div>
            <span>New</span>
        </li>
        <li>
            <div class="status_quote status_quote_seen"></div>
            <span>Seen</span>
        </li>
    </ul>

</div>
@stop