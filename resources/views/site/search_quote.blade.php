@extends('layouts.site')
@section('content')

<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>PRODUCTS</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="/">Home</a></li>
						<li>Products</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
            @include('partials.category')

        <div class="col-md-9 col-sm-7">

            <div class="row" >
            <h1>Result for " {{ $part_no }} "</h1>

            <hr />
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Product Image</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Part No.</th>
                            <th scope="col"></th>
                            
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($products_found))
                            @foreach($products_found as $product)
                                <tr>
                                    <th scope="row">
                                        <figure>
                                            <a href="/product/{{ linkURL($product->id, $product->part_no) }}"><img src="/upload/product/{{ ShowImageProduct($product->image) }}" alt="" width="80"></a>
                                        </figure>
                                    </th>
                                    <td>
                                    {{ $product->manufacturer }}
                                    </td>
                                    <td>
                                        <a href="/product/{{ linkURL($product->id, $product->part_no) }}">{{ $product->part_no }}</a>
                                    </td>
                                    <td>
                                        <span class="available">Available</span>
                                    </td>
                                    <td>
                                        <a class="button green request_button" data-value="{{ linkURL($product->id, $product->part_no) }}" >Request a Quote</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <h1>there are no products in this category</h1>
                        @endif
                    <tbody>
                </table>
                    
            </div>

            <div class="pagination-container margin-top-20" style="clear:both">
                <nav class="pagination">
                    {!! $products_found->render() !!}
                </nav>
            </div>

            <div class="margin-top-35"></div>
            <div class="form-contact">
                @include('site.partials.contact', ['contact' => false, 'quotes' => $quotes])
            </div>


        </div>
    </div>
</div>
@stop