<div class="col-md-12 col-sm-12 sticky-wrapper extra-gutter-left">
           
            {!! Form::open(['route' => 'site.store', 'id' => 'user-form']) !!}
                <table class="request_quote_form table table-striped">

                    <thead>
                    <tr>
                        <td>Part No.</td>
                        <td>Manufacturer</td>
                        <td>Quantity</td>
                        <td>Target Price</td>
                        <td>Alternates</td>
                        <td>RoHS*</td>
                        <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($quotes))
                        @foreach($quotes as $quote)
                            <tr>
                                <td><input type="text" name="part_no[]" value="{{ $quote }}" required=""> </td>
                                <td><input type="text" name="manufacturer[]"></td>
                                <td><input type="number" name="quantity[]"></td>
                                <td><input type="text" name="target_price[]"></td>
                                <td>
                                    <select name="alternates[]">
                                        <option value="">Please Select</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="rohs[]">
                                        <option value="">Please Select</option>
                                        <option value="yes">Yes</option>
                                        <option value="no">No</option>
                                    </select>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-delete btn-default" aria-label="Left Align">
                                        <i class="fa fa-remove"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                        <tr class="example">
                            <td><input type="text" name="part_no[]" value="" required=""> </td>
                            <td><input type="text" name="manufacturer[]"></td>
                            <td><input type="number" name="quantity[]"></td>
                            <td><input type="text" name="target_price[]"></td>
                            <td>
                                <select name="alternates[]">
                                    <option value="">Please Select</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </td>
                            <td>
                                <select name="rohs[]">
                                    <option value="">Please Select</option>
                                    <option value="yes">Yes</option>
                                    <option value="no">No</option>
                                </select>
                            </td>
                            <td>
                                <button type="button" class="btn btn-delete btn-default" aria-label="Left Align">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
                <input type="button" class="add-more btn btn-primary" value="Add More Part">
                

                <h3>Provide Information</h3>
                <hr>

                <table class="table table-n">
                    <tbody><tr>
                        <td>
                            <div class="form-group">
                                <label for="name">Name:</label> 
                                <input type="text" class="form-control" name="name" id="name" >   
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="phone">Phone Number:</label> 
                                <input type="text" class="form-control" name="phone" id="phone" >   
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="bussinnes_name">Business Name:</label> 
                                <input type="text" class="form-control" name="bussinnes_name" id="bussinnes_name">   
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label for="email">Email:</label> 
                                <input type="email" class="form-control" name="email" id="email" >
                            </div>   
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label for="bussinnes_type">Business Type:</label> 
                                <select name="bussinnes_type" id="bussinnes_type" class="form-control">
                                    <option value="">Please select...</option>
                                    <option value="manufacturer">Manufacturer</option>
                                    <option value="distributor">Distributor</option>
                                    <option value="government">Government</option>
                                    <option value="aerospace">Aerospace</option>
                                    <option value="service company">Service Company</option>
                                    <option value="individual person">Individual Person</option>
                                    <option value="other">Other</option>
                                </select> 
                            </div>
                        </td>
                        <td>
                            <input type="submit" class="button green"value="Request a Quote">
                        </td>
                    </tr>
                </tbody>
                </table>
            {!! Form::close() !!}
            @if($contact)
                @include('partials.contact')
            @endif
        </div>