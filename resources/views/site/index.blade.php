@extends('layouts.site')
@section('page-title', 'AllinOneComponents')
@section('content')

@include('partials.slider')

<div class="image-edge">

	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="image-edge-content">
					<h3 class="headline with-border margin-bottom-40">Why Choose Us?</h3>
					<ul>
						<li>					
							<h4><i class="fa fa-check"></i> E​LECTRONIC COMPONENT DISTRIBUTORS</h4>
							<p>Independent distributor of hard to find electronic components and electric parts. We provide a complete and cost-effective sourcing solution for all your electronic component needs. ​Find your electronic components and electric parts today. With access to our suppliers database locate your obsolete or very hard to find electronic parts, OEM parts.</p>
						</li>

						<li>					
							<h4><i class="fa fa-check"></i> TRUSTED SUPPLIER NETWORK</h4>
							<p>Our free sourcing service allows our customers to outsource the time-consuming and complex task of sourcing numerous components to our expert team. We can quickly and efficiently supply hard-to-find items as well as provide word-best pricing for all electronic components. We utilize millions of inventory data files from major OEM’s and stockists together with our word-best component search engine combined with our extensive Trusted Supplier Network to supply ¡you with a complete solution to your component procurement requirements.</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<div class="edge-bg" style="background-image: url(assets/site/images/image-edge.jpg)"></div>

</div>

<div id="counters" class="margin-top-60" style="background-image: url(assets/site/images/parallax-img.jpg)">
	<div class="container">

		<div class="row">

			<div class="col-md-12">
				<h3>WHY CHOOSE US?</h3>
			</div>

			<div class="col-md-3">
				<div class="counter-box">
					<span class="counter">578</span>
					<p>New Products</p>
				</div>
			</div>

			<div class="col-md-3">
				<div class="counter-box">
					<span class="counter">3,240,764</span>
					<p>Product Datasheets</p>
				</div>
			</div>

			<div class="col-md-3">
				<div class="counter-box">
					<span class="counter">300</span>
					<p>Reference Designs</p>
				</div>
			</div>

			<div class="col-md-3">
				<div class="counter-box last">
					<span class="counter">100</span><i>%</i>
					<p>Satisfied Customers</p>
				</div>
			</div>


		</div>

	</div>
</div>
@stop