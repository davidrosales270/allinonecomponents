@extends('layouts.site')
@section('page-title', 'AllinOneComponents - Contact Us')
@section('content')

<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>CONTACT US</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Contact Us</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
		<!-- Contact Details -->
		@include('site.partials.contact', ['contact' => true, 'quotes' => $quotes])
    </div>

</div>

@stop

@section('styles')
    {!! HTML::style('assets/site/css/contact.css') !!}
@stop
@section('scripts')
 {!! HTML::script('/assets/site/scripts/contact.js') !!}
 {!! HTML::script('vendor/jsvalidation/js/jsvalidation.js') !!}
 {!! JsValidator::formRequest('Vanguard\Http\Requests\Quote\CreateQuoteRequest', '#user-form') !!}
@stop