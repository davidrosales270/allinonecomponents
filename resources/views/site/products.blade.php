@extends('layouts.site')
@section('page-title', 'AllinOneComponents - Products')

@section('content')
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>PRODUCTS</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Products</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>



<div class="container">

	<div class="row">

	
		@include('partials.category')

		<div class="col-md-9 col-sm-12 sticky-wrapper extra-gutter-left projects">
            <div class="col-md-6">
                <a href="/product-category/main-categories/cable-wire--assemblies-16">
                    <img src="/upload/img/001.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Cable</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="/product-category/main-categories/batteries--chargers-15">
                    <img src="/upload/img/003.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content"> ,    
                            <h4>Batteries</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="product-category/main-categories/passive-components-28">
                    <img src="/upload/img/005.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Capacitors</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="/product-category/main-categories/semiconductors--discretes-33">
                    <img src="/upload/img/007.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Transistors</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
            </div>
            <div class="col-md-6">
                <a href="/product-category/main-categories/connectors-20">
                    <img src="/upload/img/009.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Connectors</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="/product-category/main-categories/optoelectronics-and-displays-27">
                    <img src="/upload/img/002.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Screens</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="/product-category/main-categories/semiconductors--ics-34">
                    <img src="/upload/img/004.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Chip</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
                <a href="/product-category/main-categories/transformers-41">
                    <img src="/upload/img/006.jpg" width="367px" height="240px" alt="">
                    <div class="overlay">
                        <div class="overlay-content">
                            <h4>Transformers</h4>
                        </div>
                    </div>
                    <div class="plus-icon"></div>
                </a>
            </div>
        </div>    

	</div>

</div>

@stop