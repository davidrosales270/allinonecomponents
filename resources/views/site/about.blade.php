@extends('layouts.site')
@section('page-title', 'AllinOneComponents - About Us')
@section('content')

<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>ABOUT US</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>About Us</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<div class="container">

	<div class="row">

		<!-- Contact Details -->
		@include('partials.contact')

		<div class="col-md-9 col-sm-12 sticky-wrapper extra-gutter-left">
            <!-- Headline -->
            <h3 class="headline small with-border">ABOUT US</h3>
            <!-- Text & Photo -->
            <div class="row margin-bottom-5">
                <!-- Text -->
                <div class="col-md-6">
                <p>
                    All In One Components, Inc. is a Worldwide Supplier of Quality Components, Parts and Equipm.  We provide a broad range 
                    of services to an ever expanding customer and market bases.  Specializing and serving the Aerospace, Telecom, Automotive,
                    and Medical industries, we have always stood by our products and customers, ensuring their satisfaction. 
                    </p>
                    <p>Our primary customers include:</p>
                <ul class="list-1">
                    <li>Defense &amp; Military</li>
                    <li>Commercial, Charter, and Cargo Airlines operating in both domestic and overseas markets</li>
                    <li>Aviation Manufacturers (commercial aircraft and corporate jet)</li>
                    <li>Aerospace Companies</li>
                    <li>Transportation</li>
                    <li>Industrial</li>
                </ul>
                </div>
                <!-- Photo -->
                <div class="col-md-6">
                <a href="assets/site/images/service-alt-01.jpg" class="img-hover mfp-image margin-top-5"><img src="assets/site/images/service-alt-01.jpg" alt=""></a>
                </div>
            </div>
            <br><br>
            <!-- OUR COMMITMENT -->
            <h3 class="headline small with-border">OUR COMMITMENT</h3>
            <!-- Tabs Content -->
            <div class="tabs-container">
                <div class="tab-content" id="tab1">
                <p>All In One Componentshas a commitment to provide 24-48 Hour RFQ Response time, On-Time Delivery, 
                    and Assurance of Quality to each of our customers.  We are committed to meeting customer requirements 
                    and enhancing customer satisfaction through continual improvement of our products, services and the 
                    quality management system; as well as new solutions to problems and reliable products at fair prices.
                    All In One Components recognizes its responsibility to conduct business in a way that protects and improves the state of 
                    the environment for future generations. We believe a strict Code of Ethics of doing business is essential for the growth 
                    and stability of our company, as well as our economy.  We are committed to customer satisfaction through our own stringent 
                    procurement procedures, and will strive to continually improve the effectiveness of our Quality Management System and 
                    our commitment to customer satisfaction by listening and constantly improving. 
                </p>
                </div>
            </div>
                <br><br>
            <!-- OUR COMMITMENT -->
            <h3 class="headline small with-border">Our Strategy</h3>
            <!-- Tabs Content -->
            <div class="tabs-container">
                <div class="tab-content" id="tab1">
                <p>
                    Our strategy has been, and will continue to be, a market position enhancement strategy: creating value for our 
                    customers and suppliers at maximum profitability. While the market and technologies may change, our strategy is to 
                    provide the highest level of service in the most cost-effective manner to our more than 100,000 customers and 4,000 
                    suppliers around the world. We deliver the Electronic Manufacturing Services, Board-Level Electronic Components and 
                    Raw Materials that our customers need to manufacture their products.
                </p>
                </div>
            </div>
            </div>

	</div>

</div>

@stop