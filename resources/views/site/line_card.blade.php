@extends('layouts.site')
@section('page-title', 'AllinOneComponents - Line Card')

@section('content')
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>LINE CARD</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Line Card</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>



<div class="container">

	<div class="row">

	
		@include('partials.category')

		<div class="col-md-9 col-sm-12 sticky-wrapper extra-gutter-left projects">
				 <h2>Manufacturers</h2>
                @foreach($_manufactures as $key=>$value)
                    <div class="line-card-box col-md-3">
                        <h3>{{ $key }}</h3>
                        <ul>
                        @for($i=0; $i < count($value); $i++)
                            <li><a href="/line-card/{{ strtolower($value[$i]) }}">{{ ucfirst($value[$i]) }}</a></li>
                        @endfor
                        </ul>
                    </div>
                @endforeach
              
        </div>    

	</div>

</div>

@stop