@extends('layouts.site')
@section('content')

<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>PRODUCTS</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="/">Home</a></li>
						<li>Products</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<div class="container">

	<div class="row">
            @include('partials.category')

        <div class="col-md-9 col-sm-7">

            <div class="row" >
                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Product Image</th>
                            <th scope="col">Part No.</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($products))
                            @foreach($products as $product)
                                <tr>
                                    <th scope="row">
                                        <figure>
                                            <a href="/product/{{ linkURL($product->id, $product->part_no) }}"><img src="/upload/product/{{ ShowImageProduct($product->image) }}" alt="" width="80"></a>
                                        </figure>
                                    </th>
                                    <td>
                                        <a href="/product/{{ linkURL($product->id, $product->part_no) }}">{{ $product->part_no }}</a>
                                    </td>
                                    <td>
                                        {{ $product->name }}
                                    </td>
                                    <td>
                                         <a class="button green" href="/contact_quotes/{{ linkURL($product->id, $product->part_no) }}">Request a Quote</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <h1>there are no products in this category</h1>
                        @endif
                    <tbody>
                </table>
                    
            </div>

            <div class="pagination-container margin-top-20" style="clear:both">
                <nav class="pagination">
                    {!! $products->render() !!}
                </nav>
            </div>
        </div>
    </div>
</div>

@stop