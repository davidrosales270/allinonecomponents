@extends('layouts.site')
@section('page-title', $product_show->part_no . ' - All in one components')
@section('content')
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>PRODUCT</h2>
				<span>ALL IN ONE COMPONENTS Inc.</span>
				
				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Product</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>


<div class="container">
    <div class="row">
        @include('partials.category')

        <div class="col-md-9 col-sm-7 extra-gutter-right">
		
            <div class="row">
                <div class="col-md-12">

                    <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Product Image</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Part No.</th>
                            <th scope="col"></th>
                            
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($product_show)
                            <tr>
                                <th scope="row">
                                    <figure>
                                        <a href="/product/{{ linkURL($product_show->id, $product_show->part_no) }}"><img src="/upload/product/{{ ShowImageProduct($product_show->image) }}" alt="" width="80"></a>
                                    </figure>
                                </th>
                                <td>
                                {{ $product_show->manufacturer }}
                                </td>
                                <td>
                                    <a href="/product/{{ linkURL($product_show->id, $product_show->part_no) }}">{{ $product_show->part_no }}</a>
                                </td>
                                <td>
                                    <span class="available"><b>Available</b></span>
                                </td>
                                <td>
                                    <a class="button green request_button" data-value="{{ linkURL($product_show->id, $product_show->part_no) }}" >Request a Quote</a>
                                </td>
                            </tr>
                            
                        @else
                            <h1>there are no products in this category</h1>
                        @endif
                    <tbody>
                </table>

                </div>
            </div>

            <div class="margin-top-35"></div>
            <div class="form-contact">
                @include('site.partials.contact', ['contact' => false, 'quotes' => $quotes])
            </div>

            <div class="row">
                <div class="col-md-12">

                    @if(count($related_products))
                    <!-- Related Products -->
                    <h4 class="headline with-border margin-top-50 margin-bottom-35">Related Products</h4>
                    <div class="row">
                        <!-- Product -->
                        @foreach($related_products as $related_product)
                        <div class="col-md-3">
                            <div class="shop-item">
                                <figure>
                                    <a href="/product/{{ linkURL($related_product->id, $related_product->part_no) }}"><img src="/upload/product/{{ ShowImageProduct($related_product->image) }}" alt="" width="80px"></a>
                                    <figcaption class="item-description">
                                        <a href="/product/{{ linkURL($related_product->id, $related_product->part_no) }}"><h5>{{ $related_product->part_no }}</h5></a>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @endif


                </div>
            </div>

        </div>
    </div>
</div>

@stop

@section('scripts')
 {!! HTML::script('/assets/site/scripts/contact.js') !!}
 {!! HTML::script('vendor/jsvalidation/js/jsvalidation.js') !!}
 {!! JsValidator::formRequest('Vanguard\Http\Requests\Quote\CreateQuoteRequest', '#user-form') !!}
@stop