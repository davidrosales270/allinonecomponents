<!-- Footer
================================================== -->
<div class="margin-top-35"></div>

<div id="footer">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6 col-xs-12">
				<h4>About</h4>
				<p>All In One Components, Inc. is a Worldwide Supplier of Quality Components, Parts and Equipm. We provide a broad range of services to an ever expanding customer and market bases.</p>
				<!--<a href="#" class="button social-btn"><i class="fa fa-facebook-official"></i> Like Us on Facebook</a>-->
			</div>

			<div class="col-md-4  col-sm-6 col-xs-12">
				<h4>Helpful Links</h4>
				<ul class="footer-links">
					<li><a href="{{ route('site') }}">Home</a></li>
					<li><a href="{{ route('about-us') }}">About Us</a></li>
					<li><a href="{{ route('products') }}">Products</a></li>
					<li><a href="{{ route('line_card')}}">Line Card</a></li>
					<li><a href="{{ route('contact')}}">Contact</a></li>
				</ul>
			</div>		

			<div class="col-md-3  col-sm-12 col-xs-12">
				<h4>Contact Us</h4>
				<div class="text-widget">
				<span>All In One Components, Inc.</span><br />
					<span>823 Osceola Trail<br /> Casselberry, FL 32707</span> <br />
					Phone: <span>407-499-2047<br /> Toll Free 844-597-2656</span><br />
					Fax: <span>321-282-7960</span> <br />
					E-Mail:<span> sales@aiocomponents.com  </span><br />
				</div>
			</div>

		</div>
		
		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights">©  Copyright {{ date('Y') }} by <a href="#">AllinOneComponents</a>. All Rights Reserved.</div>
			</div>
		</div>

	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>