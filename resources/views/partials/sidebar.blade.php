<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li class="sidebar-avatar">
                <div class="dropdown">
                    <div>
                        <img alt="image" class="img-circle" width="100" src="{{ Auth::user()->present()->avatar }}">
                    </div>
                    <div class="name"><strong>{{ Auth::user()->present()->nameOrEmail }}</strong></div>
                </div>
            </li>
            <li class="{{ Request::is('/') ? 'active open' : ''  }}">
                <a href="{{ route('dashboard') }}" class="{{ Request::is('/') ? 'active' : ''  }}">
                    <i class="fa fa-dashboard fa-fw"></i> @lang('app.dashboard')
                </a>
            </li>
            @permission('users.manage')
                <li class="{{ Request::is('user*') ? 'active open' : ''  }}">
                    <a href="{{ route('user.list') }}" class="{{ Request::is('user*') ? 'active' : ''  }}">
                        <i class="fa fa-users fa-fw"></i> @lang('app.users')
                    </a>
                </li>
            @endpermission

            @permission('users.manage')
                <li class="{{ Request::is('stock*') ? 'active open' : ''  }}">
                    <a href="{{ route('stock.list') }}" class="{{ Request::is('stock*') ? 'active' : ''  }}">
                        <i class="fa fa-server fa-fw"></i> Products
                    </a>
                </li>
            @endpermission

            @permission('users.manage')
                <li class="{{ Request::is('category*') ? 'active open' : ''  }}">
                    <a href="{{ route('category.list') }}" class="{{ Request::is('category*') ? 'active' : ''  }}">
                        <i class="fa fa-reorder fa-fw"></i> Categories
                    </a>
                </li>
            @endpermission

            @permission('users.manage')
                <li class="{{ Request::is('quote*') ? 'active open' : ''  }}">
                    <a href="{{ route('quote.list') }}" class="{{ Request::is('quote*') ? 'active' : ''  }}">
                        <i class="fa fa-book fa-fw"></i> Quotes
                    </a>
                </li>
            @endpermission

            @permission('users.manage')
                <li class="{{ Request::is('analytics*') ? 'active open' : ''  }}">
                    <a href="{{ route('analytics.home') }}" class="{{ Request::is('analytics*') ? 'active' : ''  }}">
                        <i class="fa fa-area-chart fa-fw"></i> Analytics
                    </a>
                </li>
            @endpermission

            @permission(['settings.general', 'settings.auth', 'settings.notifications'])
            <li class="{{ Request::is('settings*') ? 'active open' : ''  }}">
                <a href="#">
                    <i class="fa fa-gear fa-fw"></i> @lang('app.settings')
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    @permission('settings.general')
                        <li>
                            <a href="{{ route('settings.general') }}"
                               class="{{ Request::is('settings') ? 'active' : ''  }}">
                                @lang('app.general')
                            </a>
                        </li>
                    @endpermission
                </ul>
            </li>
            @endpermission
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>