<div class="col-md-3 sidebar-contact">
    <!-- text box -->
    <a href="{{ route('contact') }}" class="sidebar-btn">
            <i class="sl sl-icon-envolope"></i>
            <span>Request Quote</span>
        </a>

    <div class="sidebar-textbox">
        
        <h4>Our Office</h4>
        <span>All In One Components, Inc.</span><br />
        <span>823 Osceola Trail<br /> Casselberry, FL 32707</span> <br />
         <span>Phone: 407-499-2047<br /> Toll Free 844-597-2656</span><br />
         <span>Fax: 321-282-7960</span> <br />
        <span>E-Mail: sales@aiocomponents.com  </span><br />
    </div>
</div>