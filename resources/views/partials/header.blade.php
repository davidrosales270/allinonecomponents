
<!-- Header
================================================== -->

<div class="container">

	<div class="row">
		<div class="header">

			<div class="col-md-3 col-sm-12">
				<div id="logo">
					<a href="{{ route('site') }} "><img src="{{ url('assets/img/vanguard-logo.png') }}" alt="" /></a>
				</div>
			</div>

			<div class="col-md-9 col-sm-12">
			
					<div class="yith-ajaxsearchform-container">
						{!! Form::open(['route' => 'site.search_quote', 'id' => 'search-form']) !!}
							<div>
								<input type="search" value="" name="part_no" id="yith-s" class="yith-s" placeholder="Enter part here" data-loader-icon="" data-min-chars="3" autocomplete="off">

								<input type="submit" id="yith-searchsubmit" class="button green" value="Search">
							</div>
						{!! Form::close() !!}
					</div>
				
			</div>
			<div class="clearfix"></div>
			<hr />
		</div>
	</div>

</div>


<!-- Navigation
================================================== -->
<div class="container">
	<div class="row">
		<div class="col-md-7">

			<!-- Mobile Navigation -->
			<div class="menu-responsive">
				<i class="fa fa-reorder menu-trigger"></i>
			</div>
			
			<!-- Main Navigation -->
			<nav id="navigation">

				<ul class="menu" id="responsive">

					<li class="dropdown">
						<a href="{{ route('site') }}" class="{{ Request::is('/') ? 'current' : ''  }}">Home</a>
					</li>

					<li class="dropdown">
						<a href="{{ route('about-us') }}" class="{{ Request::is('about-us') ? 'current' : ''  }}">About Us</a>
					</li>

					<li class="dropdown">
						<a href="{{ route('products') }}" class="{{ Request::is('prod*') ? 'current' : '' }}">Products</a>
					</li>

					<li>
						<a href="{{ route('line_card')}}" class="{{ Request::is('line-card') ? 'current' : '' }}">Line Card</a>
					</li>

					<li>
						<a href="{{ route('contact')}}" class="{{ Request::is('contact') ? 'current' : '' }}">Contact</a>
					</li>
					<!-- Search Icon-->		
					
				</ul>
				
			</nav>
		</div>
	</div>
</div>
<div class="clearfix"></div>