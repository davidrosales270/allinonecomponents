<div class="col-md-3 sidebar-contact">
    <a href="{{ route('contact') }}" class="sidebar-btn">
        <i class="sl sl-icon-envolope"></i>
        <span>Request Quote</span>
    </a>
    <ul class="dropdown-menu">
        @foreach($categories as $category)
            <li><a href="/product-category/main-categories/{{ linkURL($category->id, $category->name) }}">{{ $category->name }}</a></li>
        @endforeach
    </ul>

    <div class="sidebar-textbox">
        <h4>Our Office</h4>
        <span>All In One Components, Inc.</span><br />
        <span>823 Osceola Trail<br /> Casselberry, FL 32707</span> <br />
        <span> Phone: 844-597-2656<br /> Toll Free 407-499-2047</span><br />
        Fax: <span>321-282-7960</span> <br />
        <span>E-Mail: sales@aiocomponents.com  </span><br />
    </div>
</div>